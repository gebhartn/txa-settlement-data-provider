// Copyright © 2023 TXA PTE. LTD.

package handler

import (
	"fmt"
	"math/big"
	"sort"

	"sdp/abis"
	"sdp/constants"
	"sdp/model"
	"sdp/store"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/math"
	"gorm.io/gorm"
)

type operation func(existingAmount *big.Int) (updatedAmount *big.Int, err error)

func (h Handler) FinalizeObligations(trader, token, settlementId string, chainId uint64) ([]abis.SettlementLibObligation, error) {
	obligations := []abis.SettlementLibObligation{}

	err := h.db.Transaction(func(tx *gorm.DB) error {
		var err error
		obligations, err = h.getLockedFundsObligations(tx, trader, token, chainId)
		if err != nil {
			return err
		}
		if err := h.markObligationAllocated(tx, trader, token, chainId, settlementId); err != nil {
			return err
		}

		owedObligations, err := h.getObligationsOwedToTrader(tx, trader, token, chainId)
		if err != nil {
			return err
		}
		obligations = append(obligations, owedObligations...)
		if err := h.markOwedObligationsSubmitted(tx, trader, token, chainId, settlementId); err != nil {
			return err
		}

		returnedObligations, err := h.getReturnedObligations(tx, trader, token, chainId)
		if err != nil {
			return err
		}
		obligations = append(obligations, returnedObligations...)
		if err := h.markReturnedObligationsSubmitted(tx, trader, token, chainId, settlementId); err != nil {
			return err
		}

		return nil
	})
	return obligations, err
}

func (h Handler) markObligationAllocated(tx *gorm.DB, trader, token string, chainId uint64, settlementId string) error {
	query := map[string]any{
		"source":    trader,
		"token":     token,
		"allocated": false,
		"chain_id":  chainId,
		"status":    model.STAGED,
	}
	return tx.
		Where(query).
		Not(&model.Obligation{Sink: trader}).
		Updates(&model.Obligation{Allocated: true, SettlementId: settlementId}).
		Error
}

func (h Handler) markOwedObligationsSubmitted(tx *gorm.DB, trader, token string, chainId uint64, settlementId string) error {
	return tx.
		Where(&model.Obligation{Sink: trader, ChainId: chainId, Token: token, Status: model.STAGED}).
		Not(&model.Obligation{Source: trader}).
		Updates(&model.Obligation{Status: model.SUBMITTED, SettlementId: settlementId}).
		Error
}

func (h Handler) markReturnedObligationsSubmitted(tx *gorm.DB, trader, token string, chainId uint64, settlementId string) error {
	return tx.
		Where(&model.Obligation{Sink: trader, Source: trader, Token: token, Allocated: true, ChainId: chainId, Status: model.STAGED}).
		Updates(&model.Obligation{Status: model.SUBMITTED, SettlementId: settlementId}).
		Error
}

func (h Handler) getReturnedObligations(tx *gorm.DB, trader, token string, chainId uint64) ([]abis.SettlementLibObligation, error) {
	os := store.NewObligationStore(tx)

	returnedObligations, err := os.FindMany(&model.Obligation{Sink: trader, Source: trader, Token: token, Allocated: true, ChainId: chainId, Status: model.STAGED}, nil)
	if err != nil {
		return nil, err
	}

	var obligations []abis.SettlementLibObligation
	for _, returned := range returnedObligations {
		obligation := abis.SettlementLibObligation{
			Amount:     math.MustParseBig256(returned.Amount),
			Deliverer:  common.HexToAddress(returned.Source),
			Recipient:  common.HexToAddress(returned.Sink),
			Token:      common.HexToAddress(returned.Token),
			Reallocate: true,
		}
		obligations = append(obligations, obligation)
	}

	return obligations, nil
}

func (h Handler) getObligationsOwedToTrader(tx *gorm.DB, trader, token string, chainId uint64) ([]abis.SettlementLibObligation, error) {
	os := store.NewObligationStore(tx)

	obligationsOwed, err := os.FindMany(&model.Obligation{Sink: trader, ChainId: chainId, Token: token, Status: model.STAGED}, &model.Obligation{Source: trader})
	if err != nil {
		return nil, err
	}

	var obligations []abis.SettlementLibObligation
	for _, owed := range obligationsOwed {
		obligation := abis.SettlementLibObligation{
			Amount:     math.MustParseBig256(owed.Amount),
			Deliverer:  common.HexToAddress(owed.Source),
			Recipient:  common.HexToAddress(owed.Sink),
			Token:      common.HexToAddress(owed.Token),
			Reallocate: owed.Allocated,
		}
		obligations = append(obligations, obligation)
	}

	return obligations, nil
}

func (h Handler) getLockedFundsObligations(tx *gorm.DB, trader, token string, chainId uint64) ([]abis.SettlementLibObligation, error) {
	os := store.NewObligationStore(tx)

	obligations := []abis.SettlementLibObligation{}
	query := map[string]any{
		"source":    trader,
		"token":     token,
		"allocated": false,
		"chain_id":  chainId,
		"status":    model.STAGED,
	}
	lockedFunds, err := os.FindManyMap(query, &model.Obligation{Sink: trader})
	if err != nil {
		return nil, err
	}

	totalOwed := big.NewInt(0)
	for _, obligation := range lockedFunds {
		totalOwed.Add(totalOwed, math.MustParseBig256(obligation.Amount))
	}

	if totalOwed.Sign() > 0 {
		obligation := abis.SettlementLibObligation{
			Amount:    totalOwed,
			Deliverer: common.HexToAddress(trader),
			Recipient: common.HexToAddress(constants.LockedFundsAddress),
			Token:     common.HexToAddress(token),
		}
		obligations = append(obligations, obligation)
	}

	return obligations, nil
}

func (h Handler) payAccount(tx *gorm.DB, deliverer, recipient, token string, chainId uint64, totalAmount *big.Int) error {
	os := store.NewObligationStore(tx)
	remainingAmount := big.NewInt(0).Set(totalAmount)

	obligations, err := os.FindMany(&model.Obligation{Sink: deliverer, Token: token, ChainId: chainId, Status: model.STAGED}, nil)
	if err != nil {
		return err
	}

	sort.Sort(model.Sortable(obligations))

	for _, obligation := range obligations {
		obligationAmount := math.MustParseBig256(obligation.Amount)

		if obligationAmount.Cmp(big.NewInt(0)) == 0 {
			continue
		}

		transferAmount := obligationAmount
		if obligationAmount.Cmp(remainingAmount) == 1 {
			// obligation amount is larger than remaining balance
			// transfer remaining amount only
			transferAmount = remainingAmount
		}

		// debit obligation amount from deliverer
		if err := h.applyDebit(tx, obligation.Source, obligation.Sink, obligation.Token, transferAmount, obligation.Allocated, obligation.ChainId); err != nil {
			return err
		}

		// credit obligation amount to recipient
		if err := h.applyCredit(tx, obligation.Source, recipient, obligation.Token, transferAmount, obligation.Allocated, obligation.ChainId); err != nil {
			return err
		}

		remainingAmount.Sub(remainingAmount, transferAmount)
		if remainingAmount.Cmp(big.NewInt(0)) == 0 {
			break
		}
	}

	if remainingAmount.Cmp(big.NewInt(0)) != 0 {
		return fmt.Errorf("total amount remaining is not 0, remaining amount: %s", remainingAmount.String())
	}

	return nil
}

func (h Handler) updateObligation(tx *gorm.DB, source, sink, token string, allocated bool, chainId uint64, op operation) error {
	os := store.NewObligationStore(tx)
	obligation, err := os.FindOne(&model.Obligation{
		Source:    source,
		Sink:      sink,
		Status:    model.STAGED,
		Token:     token,
		Allocated: allocated,
		ChainId:   chainId,
	})
	if err != nil && err != gorm.ErrRecordNotFound {
		return err
	}

	var existingAmount *big.Int
	if obligation != nil {
		existingAmount = math.MustParseBig256(obligation.Amount)
	} else {
		existingAmount = big.NewInt(0)
	}

	updatedAmount, err := op(existingAmount)
	if err != nil {
		return err
	}

	if obligation == nil {
		obligation = &model.Obligation{
			Source:    source,
			Sink:      sink,
			Token:     token,
			Allocated: allocated,
			Status:    model.STAGED,
			ChainId:   chainId,
		}
	}
	obligation.Amount = updatedAmount.String()

	if err := tx.Save(obligation).Error; err != nil {
		return err
	}
	return nil
}

func (h Handler) applyDebit(tx *gorm.DB, source, sink, token string, amount *big.Int, allocated bool, chainId uint64) error {
	if amount.Sign() <= 0 {
		return fmt.Errorf("applyDebit: attempting to debit non-positive value: %s", amount.String())
	}

	return h.updateObligation(tx, source, sink, token, allocated, chainId, func(existingAmount *big.Int) (updatedAmount *big.Int, err error) {
		if existingAmount.Cmp(amount) < 0 {
			return nil, fmt.Errorf("applyDebit: attempting to debit more than available. Available: %s, Debit: %s", existingAmount.String(), amount.String())
		}

		updatedAmount = big.NewInt(0).Sub(existingAmount, amount)
		return updatedAmount, nil
	})
}

func (h Handler) applyCredit(tx *gorm.DB, source, sink, token string, amount *big.Int, allocated bool, chainId uint64) error {
	if amount.Sign() <= 0 {
		return fmt.Errorf("applyCredit: attempting to credit non-positive value: %s", amount.String())
	}

	return h.updateObligation(tx, source, sink, token, allocated, chainId, func(existingAmount *big.Int) (*big.Int, error) {
		return big.NewInt(0).Add(existingAmount, amount), nil
	})
}
