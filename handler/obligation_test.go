// Copyright © 2023 TXA PTE. LTD.

package handler

import (
	"context"
	"math/big"
	"testing"
	"time"

	"sdp/constants"
	"sdp/model"

	"github.com/ethereum/go-ethereum/common/math"
)

func TestApplyCreditPreviousBalance(t *testing.T) {}

func TestFinalizeObligationsStatusAndId(t *testing.T) {
	h, cleanup := setup(t)
	defer cleanup(t)

	amountString := "100000000000000000000"

	for _, testCase := range []struct {
		label        string
		deliverer    string
		recipient    string
		token        string
		amount       *big.Int
		chainId      uint64
		settlementId string
		shouldFail   bool
	}{
		{
			label:        "should calculate obligations",
			deliverer:    alice,
			recipient:    bob,
			token:        constants.ZeroAddress,
			amount:       math.MustParseBig256(amountString),
			chainId:      uint64(25),
			settlementId: "99",
			shouldFail:   false,
		},
	} {
		t.Run(testCase.label, func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond*500)
			defer cancel()
			// Trade token
			if err := h.payAccount(h.db, testCase.deliverer, testCase.recipient, testCase.token, testCase.chainId, testCase.amount); err != nil {
				t.Fatal(err)
			}

			_, err := h.FinalizeObligations(testCase.deliverer, testCase.token, testCase.settlementId, testCase.chainId)
			if (err != nil) != testCase.shouldFail {
				t.Errorf("%s shouldFail %t, but got error: %v", testCase.label, testCase.shouldFail, err)
				t.FailNow()
			}

			<-ctx.Done()

			if !testCase.shouldFail {
				var m []model.Obligation

				if err := h.db.Where(&model.Obligation{
					Source:    testCase.deliverer,
					Sink:      testCase.recipient,
					Token:     testCase.token,
					Allocated: true,
					ChainId:   testCase.chainId,
				}).Find(&m).Error; err != nil {
					t.Fatal(err)
				}

				if m[0].Status != model.STAGED {
					t.Fatalf("obligation status is incorrect, wanted %s, got %s", model.STAGED, m[0].Status)
				}

				if m[0].SettlementId != testCase.settlementId {
					t.Fatalf("obligation settlementId incorrect, wanted %s, got %s", testCase.settlementId, m[0].SettlementId)
				}
			}
		})
	}
}

func TestGetLockedFundsObligations(t *testing.T) {
	// TODO: Fill me out
	t.Skip()
}

func TestGetObligationsOwedToTrader(t *testing.T) {
	// TODO: Fill me out
	t.Skip()
}

func TestGetReturnedObligations(t *testing.T) {
	// TODO: Fill me out
	t.Skip()
}

func TestMarkOwedObligationsSubmitted(t *testing.T) {
	h, cleanup := setup(t)
	defer cleanup(t)

	for _, testCase := range []struct {
		label        string
		trader       string
		token        string
		chainId      uint64
		settlementId string
		shouldFail   bool
	}{
		{
			label:        "should succeed when obligations are present",
			trader:       "2",
			token:        "0x00",
			settlementId: "1",
			chainId:      uint64(1),
			shouldFail:   false,
		},
	} {
		t.Run(testCase.label, func(t *testing.T) {
			err := h.markOwedObligationsSubmitted(h.db, testCase.trader, testCase.token, testCase.chainId, testCase.settlementId)
			if (err != nil) != testCase.shouldFail {
				t.Errorf("%s shouldFail %t, but got error: %v", testCase.label, testCase.shouldFail, err)
				t.FailNow()
			}
		})

		if !testCase.shouldFail {
			var m model.Obligation
			if err := h.db.Find(&m, "status = ?", model.SUBMITTED).Error; err != nil {
				t.Fatal(err)
			}
			if m.Sink != testCase.trader {
				t.Fatalf("expected %s, got %s", testCase.trader, m.Sink)
			}
		}
	}
}

func TestMarkReturnedObligationSubmitted(t *testing.T) {
	h, cleanup := setup(t)
	defer cleanup(t)

	for _, testCase := range []struct {
		label        string
		trader       string
		token        string
		chainId      uint64
		allocated    bool
		settlementId string
		shouldFail   bool
	}{
		{
			label:        "should succeed when obligations are present",
			trader:       "99",
			token:        "0x00",
			settlementId: "1",
			chainId:      uint64(1),
			shouldFail:   false,
		},
	} {
		t.Run(testCase.label, func(t *testing.T) {
			err := h.markOwedObligationsSubmitted(h.db, testCase.trader, testCase.token, testCase.chainId, testCase.settlementId)
			if (err != nil) != testCase.shouldFail {
				t.Errorf("%s shouldFail %t, but got error: %v", testCase.label, testCase.shouldFail, err)
				t.FailNow()
			}

			if !testCase.shouldFail {
				var m model.Obligation
				if err := h.db.Find(&m, "status = ?", model.SUBMITTED).Error; err != nil {
					t.Fatal(err)
				}
				if m.Sink != testCase.trader {
					t.Fatalf("expected %s, got %s", testCase.trader, m.Sink)
				}
			}
		})
	}
}

func TestMarkAllocated(t *testing.T) {
	h, cleanup := setup(t)
	defer cleanup(t)

	for _, testCase := range []struct {
		label        string
		trader       string
		token        string
		chainId      uint64
		allocated    bool
		settlementId string
		shouldFail   bool
	}{
		{
			label:        "should allocate obligations",
			trader:       "1",
			token:        "0x00",
			settlementId: "100",
			chainId:      uint64(1),
			shouldFail:   false,
		},
	} {
		t.Run(testCase.label, func(t *testing.T) {
			err := h.markObligationAllocated(h.db, testCase.trader, testCase.token, testCase.chainId, testCase.settlementId)
			if (err != nil) != testCase.shouldFail {
				t.Errorf("%s shouldFail %t, but got error: %v", testCase.label, testCase.shouldFail, err)
				t.FailNow()
			}

			if !testCase.shouldFail {
				var m model.Obligation
				if err := h.db.Find(&m, "allocated = ? AND settlement_id = ?", true, testCase.settlementId).Error; err != nil {
					t.Fatal(err)
				}

				if m.Allocated != true {
					t.Fatal("expected obligation to be allocated")
				}

				if m.SettlementId != testCase.settlementId {
					t.Fatal("expected obligation to have settlement id")
				}
			}
		})
	}
}

func TestPayAccount(t *testing.T) {
	h, cleanup := setup(t)
	defer cleanup(t)

	for _, testCase := range []struct {
		label       string
		deliverer   string
		recipient   string
		token       string
		chainId     uint64
		totalAmount *big.Int
		shouldFail  bool
	}{
		{
			label:       "should succeed when valid deposits exist",
			deliverer:   "55",
			recipient:   "56",
			token:       "0x00",
			chainId:     uint64(1),
			totalAmount: big.NewInt(100),
			shouldFail:  false,
		},
		{
			label:       "should succeed when recipient does not yet exist",
			deliverer:   "55",
			recipient:   "59",
			token:       "0x00",
			chainId:     uint64(1),
			totalAmount: big.NewInt(100),
			shouldFail:  false,
		},
		{
			label:       "should fail when valid deposits do not exist",
			deliverer:   "57",
			recipient:   "55",
			token:       "0x00",
			chainId:     uint64(1),
			totalAmount: big.NewInt(100),
			shouldFail:  true,
		},
		{
			label:       "should fail when existing amount does not cover obligation",
			deliverer:   "55",
			recipient:   "59",
			token:       "0x00",
			chainId:     uint64(1),
			totalAmount: big.NewInt(100_000_000),
			shouldFail:  true,
		},
	} {
		t.Run(testCase.label, func(t *testing.T) {
			err := h.payAccount(h.db, testCase.deliverer, testCase.recipient, testCase.token, testCase.chainId, testCase.totalAmount)
			if (err != nil) != testCase.shouldFail {
				t.Errorf("%s shouldFail %t, but got error: %v", testCase.label, testCase.shouldFail, err)
				t.FailNow()
			}

			if !testCase.shouldFail {
				var m model.Obligation
				if err := h.db.Find(&m, "sink = ? AND source = ? AND token = ?", testCase.recipient, testCase.deliverer, testCase.token).Error; err != nil {
					t.Fatal(err)
				}

				if m.Amount != testCase.totalAmount.String() {
					t.Fatalf("expected %s but got %s", testCase.totalAmount.String(), m.Amount)
				}
			}
		})
	}
}

func TestApplyDebit(t *testing.T) {
	h, cleanup := setup(t)
	defer cleanup(t)

	for _, testCase := range []struct {
		label      string
		source     string
		sink       string
		token      string
		amount     *big.Int
		allocated  bool
		chainId    uint64
		shouldFail bool
	}{
		{
			label:      "should update obligation if it does exist",
			source:     "1",
			sink:       "2",
			token:      "0x00",
			amount:     big.NewInt(100),
			allocated:  false,
			chainId:    uint64(1),
			shouldFail: false,
		},
		{
			label:      "should fail if existing obligation cannot cover debit amount",
			source:     "1",
			sink:       "2",
			token:      "0x00",
			amount:     big.NewInt(5000),
			allocated:  false,
			chainId:    uint64(1),
			shouldFail: true,
		},
		{
			label:      "should fail if obligation does not exist",
			source:     "11",
			sink:       "22",
			token:      "0x0001",
			amount:     big.NewInt(5000),
			allocated:  false,
			chainId:    uint64(1),
			shouldFail: true,
		},
	} {
		t.Run(testCase.label, func(t *testing.T) {
			err := h.applyDebit(h.db, testCase.source, testCase.sink, testCase.token, testCase.amount, testCase.allocated, testCase.chainId)
			if (err != nil) != testCase.shouldFail {
				t.Errorf("%s shouldFail %t, but got error: %v", testCase.label, testCase.shouldFail, err)
				t.FailNow()
			}

			// TODO: Query DB directly for assertion results
		})
	}
}

func TestApplyCredit(t *testing.T) {
	h, cleanup := setup(t)
	defer cleanup(t)

	for _, testCase := range []struct {
		label      string
		source     string
		sink       string
		token      string
		amount     *big.Int
		allocated  bool
		chainId    uint64
		shouldFail bool
	}{
		{
			label:      "should update obligation if it does exist",
			source:     "1",
			sink:       "2",
			token:      "0x00",
			amount:     big.NewInt(100),
			allocated:  false,
			chainId:    uint64(1),
			shouldFail: false,
		},
		{
			label:      "should create obligation if it does not exist",
			source:     "15",
			sink:       "16",
			token:      "0x00",
			amount:     big.NewInt(40000),
			allocated:  false,
			chainId:    uint64(1),
			shouldFail: false,
		},
		{
			label:   "apply credit no balance",
			source:  "0x698a91D825A6ab5Ad56b4CB81324Dd2fCdA02d61",
			sink:    "0xa98608103E8AA647bC5b980d018b011d91ab5DCF",
			token:   constants.ZeroAddress,
			amount:  math.MustParseBig256("100000000000000000000"),
			chainId: uint64(25),
		},
	} {
		t.Run(testCase.label, func(t *testing.T) {
			err := h.applyCredit(h.db, testCase.source, testCase.sink, testCase.token, testCase.amount, testCase.allocated, testCase.chainId)
			if (err != nil) != testCase.shouldFail {
				t.Errorf("%s shouldFail %t, but got error: %v", testCase.label, testCase.shouldFail, err)
				t.FailNow()
			}

			// TODO: Query DB directly for assertion results
		})
	}
}
