// Copyright © 2023 TXA PTE. LTD.

package handler

import (
	"log"
	"testing"

	"sdp/constants"
	"sdp/model"
)

func TestProcessOrder(t *testing.T) {
	_, cleanup := setup(t)
	defer cleanup(t)

	orderMsg := &model.OrdersCanceledForSettlement{
		Address:           constants.ZeroAddress,
		ChainId:           17001,
		SettlementId:      "2",
		SdpSequenceNumber: 1,
	}

	for _, testCase := range []struct {
		label      string
		order      *model.OrdersCanceledForSettlement
		shouldFail bool
	}{
		{
			label: "should process order",
			order: orderMsg,
			// TODO: Reporting always fails! Need a better way to stub ethClient for tests
			shouldFail: true,
		},
	} {
		log.Println(testCase)
	}
}
