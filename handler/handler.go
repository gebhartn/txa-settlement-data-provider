// Copyright © 2023 TXA PTE. LTD.

package handler

import (
	"context"
	"time"

	"sdp/blockchain"
	"sdp/constants"
	"sdp/db"
	"sdp/logger"
	"sdp/queue"
	"sdp/wsconn"

	"github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

const (
	tickerDuration    = time.Millisecond * 5
	maxTickerDuration = time.Second * 33
)

type Handler struct {
	db         *gorm.DB
	queue      *queue.Queue
	controller *wsconn.Controller
	reporters  map[string]blockchain.BlockchainClient
	log        *logrus.Logger
	listener   *pq.Listener
	tradeChan  chan *pq.Notification
	orderChan  chan *pq.Notification
	eventChan  chan *pq.Notification
}

func NewHandler(d *gorm.DB, queue *queue.Queue, controller *wsconn.Controller) *Handler {
	logger := logger.Default
	listener := db.NewListener()

	if err := listener.Listen(constants.TRADE_CHANNEL); err != nil {
		logger.Fatal(err)
	}
	if err := listener.Listen(constants.ORDER_CHANNEL); err != nil {
		logger.Fatal(err)
	}
	if err := listener.Listen(constants.EVENT_CHANNEL); err != nil {
		logger.Fatal(err)
	}

	return &Handler{
		db:         d,
		queue:      queue,
		controller: controller,
		reporters:  blockchain.GetReporters(),
		log:        logger,
		listener:   listener,
		tradeChan:  make(chan *pq.Notification),
		orderChan:  make(chan *pq.Notification),
		eventChan:  make(chan *pq.Notification),
	}
}

func (h *Handler) Start(ctx context.Context) {
	defer close(h.tradeChan)
	defer close(h.orderChan)
	defer close(h.eventChan)
	for {
		select {
		case <-ctx.Done():
			return
		case notification := <-h.listener.Notify:
			switch notification.Channel {
			case constants.TRADE_CHANNEL:
				h.tradeChan <- notification
			case constants.ORDER_CHANNEL:
				h.orderChan <- notification
			case constants.EVENT_CHANNEL:
				h.eventChan <- notification
			}
		}
	}
}

func (h *Handler) logException(err error) {
	logger.CaptureException(err)
	h.log.Error(err)
}
