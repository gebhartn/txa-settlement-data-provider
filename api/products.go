// Copyright © 2023 TXA PTE. LTD.

package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"

	"sdp/config"
	"sdp/constants"
	"sdp/logger"
	"sdp/model"
	"sdp/retry"
	"sdp/types"
)

var (
	products        = make(types.Products)
	getProductsOnce sync.Once
	log             = logger.Default
)

func fetchProducts() *types.Products {
	var res *http.Response
	var pr *types.Products
	var err error

	action := func(_ uint) error {
		if res, err = http.Get(config.GetWsUrlWithTransport() + "/products"); err != nil {
			log.Error(err)
			return err
		}
		return nil
	}
	if err := retry.Do(action); err != nil {
		log.Error(err)
		panic(err)
	}

	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}

	if err = json.Unmarshal(body, &pr); err != nil {
		panic(err)
	}

	return pr
}

func fetchAsset(id string) *types.ApiAsset {
	var a *types.ApiAsset
	var res *http.Response
	var err error

	action := func(_ uint) error {
		if res, err = http.Get(config.GetInternalApi() + "/assets/" + id); err != nil {
			log.Error(err)
			return err
		}
		return nil
	}
	if err := retry.Do(action); err != nil {
		log.Error(err)
		panic(err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}

	if err = json.Unmarshal(body, &a); err != nil {
		panic(err)
	}

	lc := strings.ToLower(a.ContractAddress)
	a.ContractAddress = lc

	return a
}

// GetProducts will query the Internal API for product IDs and iterate the returned products to fetch individual assets.
// These assets are supported by the SDP and need only be fetched once at startup.
func GetProducts() types.Products {
	getProductsOnce.Do(func() {
		if config.GetTestFlag() {
			log.Info("Using TEST products")
			products[constants.ETHUSDT] = &types.Product{
				BaseAsset: types.ApiAsset{
					ContractAddress: constants.ZeroAddress,
					EvmChainId:      5,
					Precision:       10,
					NativePrecision: 18,
				},
				CounterAsset: types.ApiAsset{
					ContractAddress: constants.UsdtGoerliAddress,
					EvmChainId:      5,
					Precision:       10,
					NativePrecision: 18,
				},
			}
			products[constants.ETHDMC] = &types.Product{
				BaseAsset: types.ApiAsset{
					ContractAddress: constants.ZeroAddress,
					EvmChainId:      17001,
					Precision:       6,
					NativePrecision: 18,
				},
				CounterAsset: types.ApiAsset{
					ContractAddress: constants.DmcAddress,
					EvmChainId:      17001,
					Precision:       6,
					NativePrecision: 6,
				},
			}
		} else {
			log.Info("Using Internal API products")
			products = *fetchProducts()
		}
	})
	return products
}

// TODO @gebhartn: Support limit/offset in SDP streamer
func GetTrade(id string, _, _ int) (*model.Trade, error) {
	var res *http.Response
	var trade *model.Trade
	var err error

	action := func(_ uint) error {
		if res, err = http.Get(config.GetWsUrlWithTransport() + "/trades/" + id); err != nil {
			log.Error(err)
			return err
		}
		return nil
	}
	if err := retry.Do(action); err != nil {
		return nil, err
	}

	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if err = json.Unmarshal(body, &trade); err != nil {
		return nil, err
	}

	return trade, nil
}
