// Copyright © 2023 TXA PTE. LTD.

package retry

import (
	"time"

	"sdp/constants"

	"github.com/Rican7/retry"
	"github.com/Rican7/retry/backoff"
	"github.com/Rican7/retry/strategy"
)

// Do executes with environment-set limit and backoff.
func Do(action func(attempt uint) error) error {
	return retry.Retry(action, strategy.Limit(constants.MAX_CONNECTION_RETRIES), strategy.Backoff(backoff.BinaryExponential(500*time.Millisecond)))
}

// DoWithLimitAndBackoff executes with limit and backoff (expressed in Duration) provided in params
func DoWithLimitAndFixedBackoff(action retry.Action, l uint, d time.Duration) error {
	return retry.Retry(action, strategy.Limit(l), strategy.Wait(d))
}

func DoFixedBackoff(action retry.Action, d time.Duration) error {
	return retry.Retry(action, strategy.Wait(d))
}
