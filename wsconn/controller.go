// Copyright © 2023 TXA PTE. LTD.

package wsconn

import (
	"context"
	"encoding/json"
	"fmt"

	"sdp/config"
	"sdp/constants"
	"sdp/logger"
	"sdp/queue"
	"sdp/types"

	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
)

type Controller struct {
	clients map[string]*WS
	orders  chan string
	trades  chan string
	queue   *queue.Queue
	closing chan chan error
	logger  *logrus.Logger
}

// TODO: Implement graceful shutdown
func NewController(queue *queue.Queue) *Controller {
	clients := make(map[string]*WS)
	ethDmc := newWS(config.GetWsURL())
	clients[constants.ETHDMC] = ethDmc

	controller := &Controller{
		queue:   queue,
		clients: clients,
		orders:  make(chan string),
		trades:  make(chan string),
		closing: make(chan chan error),
		logger:  logger.Default,
	}

	return controller
}

func (w *Controller) Close() error {
	errc := make(chan error)
	w.closing <- errc
	return <-errc
}

func (c *Controller) Start(ctx context.Context) {
	var err error
	go func() {
		for _, client := range c.clients {
			go client.PingAndReconnect()
			go func(conn *WS) {
				var mt int
				var m []byte
				for {
					mt, m, err = conn.Conn.ReadMessage()
					if err != nil {
						if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
							c.logger.Errorf("error reading message from client: %v", conn.host)
						}
						if err = conn.reconnect(); err != nil {
							c.logger.Errorf("reconnect failed: %v", err)
						}
					}

					msgType := new(types.MsgType)
					if mt != -1 {
						msgType, err = msgType.Decode(m)
						if err != nil {
							c.logger.Errorf("error decoding message: %v", err)
							continue
						}

						// Add the message to the buffer
						switch *msgType {
						case types.MSG_TRADE:
							if err = c.queue.Publish(context.Background(), config.GetTradeQueueKey(), string(m)); err != nil {
								c.logger.Errorf("error publishing trade: %v", err)
								return
							}
						case types.MSG_ORDERS_CANCELED_FOR_SETTLEMENT:
							if err = c.queue.Publish(context.Background(), config.GetOrderQueueKey(), string(m)); err != nil {
								c.logger.Errorf("error publishing order: %v", err)
								return
							}
						default:
						}
					}
				}
			}(client)
		}
	}()

	for {
		select {
		case <-ctx.Done():
			return
		case errc := <-c.closing:
			errc <- err
			return
		}
	}
}

func (c *Controller) Broadcast(seq uint64) {
	for _, ws := range c.clients {
		e := new(event).makeEvent(seq)
		ws.mu.Lock()
		ws.Conn.WriteMessage(websocket.TextMessage, e)
		ws.mu.Unlock()
	}
}

type event struct {
	Event             string `json:"event"`
	SdpSequenceNumber string `json:"sdpSequenceNumber"`
}

func (e *event) makeEvent(s uint64) []byte {
	e.SdpSequenceNumber = fmt.Sprintf("%d", s)
	e.Event = "query"
	b, _ := json.Marshal(e)
	return b
}
