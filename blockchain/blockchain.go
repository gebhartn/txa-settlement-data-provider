// Copyright © 2023 TXA PTE. LTD.

package blockchain

import (
	"context"
	"crypto/ecdsa"
	"encoding/json"
	"fmt"
	"math/big"
	"strings"
	"time"

	"sdp/abis"
	"sdp/config"
	"sdp/constants"
	"sdp/logger"
	"sdp/merkle"
	"sdp/model"
	"sdp/retry"

	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/math"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ethereum/go-ethereum/params"
	"github.com/ethereum/go-ethereum/rpc"
	"github.com/sirupsen/logrus"
)

type AccountData struct {
	Address string `json:"address"`
	PrivKey string `json:"privkey"`
}

type BlockchainClient struct {
	rpcClient               *rpc.Client
	ethClient               *ethclient.Client
	consensusAbi            abi.ABI
	identityRegistryAbi     abi.ABI
	identityRegistryAddress common.Address
	consensusAddress        common.Address
	coordinatorAddress      common.Address
	account                 AccountData
	logger                  *logrus.Logger
}

type ReportsObligations interface {
	ReportObligations(event *model.Event, obligations []abis.SettlementLibObligation) error
}

func NewBlockchainClient(rpcUrl string, identityRegistryAddress string, consensusAddress string, coordinatorAddress string) BlockchainClient {
	rpcClient, err := rpc.Dial(rpcUrl)
	logger := logger.Default
	if err != nil {
		logger.Fatalf("Error connecting RPC client: %v", err)
	}

	ethClient, err := ethclient.Dial(rpcUrl)
	if err != nil {
		logger.Fatalf("Error connecting eth client: %v", err)
	}

	consensusAbi, err := abi.JSON(strings.NewReader(abis.ConsensusMetaData.ABI))
	if err != nil {
		logger.Fatalf("Failed to read oracle contract abi: %v", err)
	}

	identityRegistryAbi, err := abi.JSON(strings.NewReader(abis.IdentityRegistryMetaData.ABI))
	if err != nil {
		logger.Fatalf("Failed to read oracle master contract abi: %v", err)
	}

	account := loadEnvAccount(*ethClient)

	return BlockchainClient{
		rpcClient:               rpcClient,
		ethClient:               ethClient,
		consensusAbi:            consensusAbi,
		identityRegistryAbi:     identityRegistryAbi,
		identityRegistryAddress: common.HexToAddress(identityRegistryAddress),
		consensusAddress:        common.HexToAddress(consensusAddress),
		coordinatorAddress:      common.HexToAddress(coordinatorAddress),
		account:                 account,
		logger:                  logger,
	}
}

func loadEnvAccount(_ ethclient.Client) AccountData {
	privKeyStr := config.GetPrivKey()
	logger := logger.Default
	privateKey, err := crypto.HexToECDSA(privKeyStr)
	if err != nil {
		logger.Fatal(err)
	}

	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		logger.Fatal("error casting public key to ECDSA")
	}

	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	logger.Infof("Loaded private key for address: %v", fromAddress)
	return AccountData{fromAddress.String(), privKeyStr}
}

// Given a chain id and settlement id, query blockchain for settlement info
func (client BlockchainClient) CheckSettlement(settlementId string) (*abis.CoordinationInterfaceSettlementData, error) {
	caller, err := abis.NewCoordinatorCaller(client.coordinatorAddress, client.ethClient)
	if err != nil {
		client.logger.Errorf("error creating coord caller: %v", err)
		return nil, err
	}
	reqData, err := caller.GetReqData(nil, math.MustParseBig256(settlementId))
	if err != nil {
		client.logger.Errorf("error calling coord for reqData: %v", err)
		return nil, err
	}
	return &reqData, nil
}

// Submits transaction to report obligations
func (client BlockchainClient) ReportObligations(event *model.Event, obligations []abis.SettlementLibObligation) error {
	// Construct merkle tree from obligations
	merkleRoot, _ := merkle.GenerateMerkleTree(obligations)

	// Pack function arguments
	args := struct {
		Coordinator  common.Address
		SettlementId *big.Int
		Obligations  []abis.SettlementLibObligation
		MerkleRoot   [32]byte
		WorkNonce    *big.Int
	}{
		Coordinator:  client.coordinatorAddress,
		SettlementId: math.MustParseBig256(event.SettlementId),
		Obligations:  obligations,
		MerkleRoot:   merkleRoot,
		WorkNonce:    math.MustParseBig256("0"),
	}
	data, err := client.consensusAbi.Pack("reportSettlementObligations",
		args.Coordinator,
		args.SettlementId,
		args.Obligations,
		args.MerkleRoot,
		args.WorkNonce,
	)
	if err != nil {
		return fmt.Errorf("error packing arguments for call to reportObligations: %v", err)
	}

	// Estimate gas needed
	gas, err := client.ethClient.EstimateGas(context.Background(), ethereum.CallMsg{
		From:     common.HexToAddress(client.account.Address),
		To:       &client.consensusAddress,
		Data:     data,
		Value:    big.NewInt(0),
		GasPrice: client.getGasPrice(),
	})
	if err := client.handleErrorEstimatingGas(err, args.SettlementId); err != nil {
		if strings.Contains(err.Error(), "settlement was already reported") {
			return nil
		}
		block, err := client.ethClient.BlockNumber(context.Background())
		if err != nil {
			return err
		}
		json, err := json.Marshal(args)
		if err != nil {
			return err
		}

		err = fmt.Errorf("error estimating gas for block %d with txData %v", block, json)
		client.logger.Error(err)
		logger.CaptureException(err)

		return err
	}

	// Create transaction
	privateKey, err := crypto.HexToECDSA(client.account.PrivKey)
	if err != nil {
		return fmt.Errorf("error parsing private key: %v", err)
	}
	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		return fmt.Errorf("error casting public key to ECDSA")
	}
	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	nonce, err := client.ethClient.PendingNonceAt(context.Background(), fromAddress)
	if err != nil {
		return fmt.Errorf("error getting nonce for account: %v", err)
	}
	tx := types.NewTransaction(nonce, client.consensusAddress, big.NewInt(0), gas, client.getGasPrice(), data)

	// Sign and send transaction
	chainID, err := client.ethClient.NetworkID(context.Background())
	if err != nil {
		return fmt.Errorf("error getting network ID: %v", err)
	}
	signedTx, err := types.SignTx(tx, types.NewEIP155Signer(chainID), privateKey)
	if err != nil {
		return fmt.Errorf("error signing transaction: %v", err)
	}
	if err := client.sendTransaction(context.Background(), signedTx); err != nil {
		return fmt.Errorf("error sending transaction to report obligations: %v", err)
	}

	// Check transaction receipt
	if err := client.getTransactionReceipt(signedTx.Hash()); err != nil {
		return fmt.Errorf("error requesting transaction receipt: %v", err)
	}

	return nil
}

func (client BlockchainClient) sendTransaction(ctx context.Context, tx *types.Transaction) error {
	return retry.DoWithLimitAndFixedBackoff(func(uint) error {
		return client.ethClient.SendTransaction(context.Background(), tx)
	}, 30, 2)
}

// handles the errors resulting from calls to estimate gas -- this could be caused by always failing transaction
// or because settlement was already reported - so do some checks before determining it is an outright failure
func (client BlockchainClient) handleErrorEstimatingGas(err error, settlementId *big.Int) error {
	if err == nil {
		return nil
	}

	client.logger.Infof("Received an error estimating gas, checking for INVALID_ID")
	caller, err := abis.NewCoordinatorCaller(client.coordinatorAddress, client.ethClient)
	if err != nil {
		return fmt.Errorf("error creating coord caller: %v", err)
	}

	lastSettlementId, err := caller.LastSettlementIdProcessed(nil)
	if err != nil {
		return fmt.Errorf("error getting last settlement id processed: %v", err)
	}

	client.logger.Infof("Last Settlement Id: %v  Event Settlement Id: %v", lastSettlementId.String(), settlementId.String())
	// This settlement was already reported for, do not report
	if lastSettlementId.Cmp(settlementId) >= 0 {
		return fmt.Errorf("settlement was already reported")
	}

	return nil
}

// gets the current gas price from the Ethereum client.
// if it fails, it returns a default gas price of 200 gwei.
func (client BlockchainClient) getGasPrice() *big.Int {
	gasPrice, err := client.ethClient.SuggestGasPrice(context.Background())
	if err != nil {
		client.logger.Errorf("error obtaining suggested gas price: %v", err)
		client.logger.Warn("reverting to default gas price of 200 gwei")
		gasPrice = new(big.Int).Mul(big.NewInt(200), big.NewInt(params.GWei))
	}
	return gasPrice
}

func (client BlockchainClient) getTransactionReceipt(hash common.Hash) error {
	action := func(_ uint) error {
		tx, isPending, err := client.ethClient.TransactionByHash(context.Background(), hash)
		if tx == nil {
			return fmt.Errorf("transaction receipt not found")
		}
		if !isPending {
			client.logger.Infof("Transaction hash %v included", tx.Hash())
			return nil
		}
		return err
	}
	return retry.DoWithLimitAndFixedBackoff(action, 30, time.Second*2)
}

// GetReporters will return a map from chainID : Reporter
func GetReporters() map[string]BlockchainClient {
	chainHosts := config.GetRPCHosts(constants.Chains)
	contractAddresses := config.GetContractAddresses(constants.Chains)
	reporters := make(map[string]BlockchainClient)

	for chain, hostPair := range chainHosts {
		var urlPrimary string
		var urlFallback string
		if config.GetRPCPort() == "0" {
			urlPrimary = fmt.Sprintf("%v://%v", config.GetRPCTransport(), hostPair.Primary)
			urlFallback = fmt.Sprintf("%v://%v", config.GetRPCTransport(), hostPair.Fallback)
		} else {
			urlPrimary = fmt.Sprintf("%v://%v:%v", config.GetRPCTransport(), hostPair.Primary, config.GetRPCPort())
			urlFallback = fmt.Sprintf("%v://%v:%v", config.GetRPCTransport(), hostPair.Fallback, config.GetRPCPort())
		}
		contracts := contractAddresses[chain]
		reporters[chain] = NewBlockchainClient(
			urlPrimary,
			contracts.IdentityRegistryAddress.Hex(),
			contracts.ConsensusAddress.Hex(),
			contracts.CoordinatorAddress.Hex(),
		)
		reporters[chain+"_fallback"] = NewBlockchainClient(
			urlFallback,
			contracts.IdentityRegistryAddress.Hex(),
			contracts.ConsensusAddress.Hex(),
			contracts.CoordinatorAddress.Hex(),
		)
	}
	return reporters
}
