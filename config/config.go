// Copyright © 2023 TXA PTE. LTD.

package config

import (
	"fmt"
	"os"
	"strconv"

	"github.com/ethereum/go-ethereum/common"
)

type ContractAddresses struct {
	IdentityRegistryAddress common.Address
	ConsensusAddress        common.Address
	CoordinatorAddress      common.Address
}

type HostPair struct {
	Primary  string
	Fallback string
}

func readEnvVar(name string, defaultValue string) string {
	if envVarValue, exists := os.LookupEnv(name); exists {
		return envVarValue
	}
	return defaultValue
}

// GetGrpcPort if set then returns the grpc port or defaults to 50051
func GetGrpcPort() string {
	return readEnvVar("GRPC_PORT", "50051")
}

// GetRPCTransport if RPC_TRANSPORT set then returns the rpc transport type from
// environment or defaults to http
func GetRPCTransport() string {
	return readEnvVar("RPC_TRANSPORT", "http")
}

// GetRPCHost if RPC_HOST is set then returns the rpc_host from the environment
// or defaults to localhost
func GetRPCHost() string {
	return readEnvVar("RPC_HOST", "localhost")
}

// GetRPCHosts collects RPC hostnames for a list of configured RPC
func GetRPCHosts(chains []string) map[string]HostPair {
	ret := make(map[string]HostPair)
	for _, chain := range chains {
		primary := readEnvVar(fmt.Sprintf("RPC_HOST_%s", chain), "localhost")
		pair := HostPair{
			Primary:  primary,
			Fallback: readEnvVar(fmt.Sprintf("RPC_HOST_%s_FALLBACK", chain), primary),
		}
		ret[chain] = pair
	}
	return ret
}

// GetRPCHosts collects RPC hostnames for a list of configured RPC
func GetContractAddresses(chains []string) map[string]ContractAddresses {
	ret := make(map[string]ContractAddresses)
	for _, chain := range chains {
		ret[chain] = ContractAddresses{
			common.HexToAddress(readEnvVar(fmt.Sprintf("IDENTITY_REGISTRY_%s", chain), "0xD588C5E2874abd8BBF515755a9C0f9F7271B2953")),
			common.HexToAddress(readEnvVar(fmt.Sprintf("CONSENSUS_%s", chain), "0x7aa2a80e4806754Db209eeAA3F82A050F63eC45A")),
			common.HexToAddress(readEnvVar(fmt.Sprintf("COORDINATOR_%s", chain), "0x41Ae37E15B1D8F1d525B513467F3523E4cA31120")),
		}
	}
	return ret
}

// GetRPCPort if RPC_PORT is set then returns the RPC_PORT from the environment
// or defaults to 8545
func GetRPCPort() string {
	return readEnvVar("RPC_PORT", "8545")
}

// GetWsHost if WEBSOCKETS_HOST is set then returns the WEBSOCKETS_HOST from
// then environment or defaults to localhost
func GetWsHost() string {
	return readEnvVar("WEBSOCKETS_HOST", "localhost")
}

// GetWsPort if WEBSOCKETS_PORT is set then returns the WEBSOCKETS_PORT from
// then environment or defaults to 8052
func GetWsPort() string {
	return readEnvVar("WEBSOCKETS_PORT", "8052")
}

// GetRedisHost if REDIS_HOST is set then returns the REDIS_HOST from
// then environment or defaults to localhost
func GetRedisHost() string {
	return readEnvVar("REDIS_HOST", "localhost")
}

// GetRedisPort if REDIS_PORT is set then returns the REDIS_PORT from
// then environment or defaults to 6379
func GetRedisPort() string {
	return readEnvVar("REDIS_PORT", "6379")
}

// GetRedisPassword if REDIS_PASSWORD is set then returns the REDIS_PASSWORD from
// then environment or defaults to "password"
func GetRedisPassword() string {
	return readEnvVar("REDIS_PASSWORD", "password")
}

// GetPgHost returns the hostname for postgres
func GetPgHost() string {
	return readEnvVar("PSQL_HOST", "localhost")
}

// GetPgUser returns the username for postgres
func GetPgUser() string {
	return readEnvVar("PSQL_USER", "postgres")
}

// GetPgPassword returns the password for postgres
func GetPgPassword() string {
	return readEnvVar("PSQL_PASSWORD", "password")
}

// GetPgPort returns the port for postgres
func GetPgPort() string {
	return readEnvVar("PSQL_PORT", "5432")
}

// GetPgDb returns the database name for postgres
func GetPgDb() string {
	return readEnvVar("PSQL_DATABASE", "postgres")
}

// GetPgDb returns the full DSN for postgres
func GetPgURL() string {
	return fmt.Sprintf("postgresql://%s:%s@%s:%s/%s", GetPgUser(), GetPgPassword(), GetPgHost(), GetPgPort(), GetPgDb())
}

// GetIdentityRegistryAddress returns the Identity Registry's Address
func GetIdentityRegistryAddress() string {
	return readEnvVar("IDENTITY_REGISTRY_ADDRESS", "0xD588C5E2874abd8BBF515755a9C0f9F7271B2953")
}

// GetConsensusAddress returns the Consensus Address
func GetConsensusAddress() string {
	return readEnvVar("CONSENSUS_ADDRESS", "0x7aa2a80e4806754Db209eeAA3F82A050F63eC45A")
}

// GetCoordinatorAddress returns the Coordinator Address
func GetCoordinatorAddress() string {
	return readEnvVar("COORDINATOR_ADDRESS", "0x41Ae37E15B1D8F1d525B513467F3523E4cA31120")
}

// GetRedisURL  returns combination of REDIS_HOST and REDIS_PORT as REDIS_HOST:REDIS_PORT
// for a complete Redis URL
func GetRedisURL() string {
	return fmt.Sprintf("%v:%v", GetRedisHost(), GetRedisPort())
}

// GetRPCURL returns RPC address by combining RPC_TRANSPORT, RPC_HOST and RPC_PORT
// to be RPC_TRANSPORT://RPC_HOST:RPC_PORT
func GetRPCURL() string {
	return fmt.Sprintf("%v://%v:%v", GetRPCTransport(), GetRPCHost(), GetRPCPort())
}

// GetWsURL returns the web socket URL by combining WS_HOST and WS_PORT
// to be WS_HOST:WS_PORT
func GetWsURL() string {
	return fmt.Sprintf("%v:%v", GetWsHost(), GetWsPort())
}

func GetWsUrlWithTransport() string {
	return fmt.Sprintf("http://%v", GetWsURL())
}

// GetTestFlag returns the test flag if it is set or FALSE
func GetTestFlag() bool {
	testFlag := readEnvVar("TEST", "FALSE")
	flag, err := strconv.ParseBool(testFlag)
	if err != nil {
		panic(err)
	}
	return flag
}

// GetPrivKey gets a private key for the blockchain client
func GetPrivKey() string {
	// ASSUME BELOW KEY IS COMPROMISED; DO NOT USE IN PRODUCTION
	return readEnvVar("PRIV_KEY", "4653831146b7fdbcb634daa0ec2a6df9319b9f5f93709868d327eab2b71abdac")
}

func GetBlockchainEventsKey() string {
	return readEnvVar("BLOCKCHAIN_EVENTS_KEY", "queue:blockchain_events")
}

func GetTradeQueueKey() string {
	return readEnvVar("TRADE_QUEUE_KEY", "queue:trades")
}

func GetOrderQueueKey() string {
	return readEnvVar("ORDER_QUEUE_KEY", "queue:orders")
}

func GetHttpAddress() string {
	a := readEnvVar("HTTP_ADDRESS", "4000")
	return ":" + a
}

func GetInternalApi() string {
	return readEnvVar("INTERNAL_API_URL", "http://localhost:4000")
}

func GetRetryHardStop() int {
	i, err := strconv.Atoi(readEnvVar("RETRY_HARD_STOP", "1800"))
	if err != nil {
		panic(err)
	}
	return i
}

func GetSentryEnabled() bool {
	enabled := readEnvVar("SENTRY_ENABLED", "false")
	flag, err := strconv.ParseBool(enabled)
	if err != nil {
		panic(err)
	}
	return flag
}

func GetSentryDsn() string {
	return readEnvVar("SENTRY_DSN", "")
}

func GetAppName() string {
	return readEnvVar("APP_NAME", "sdp-ethusdt")
}

func GetAppEnv() string {
	return readEnvVar("APP_ENV", "development")
}
