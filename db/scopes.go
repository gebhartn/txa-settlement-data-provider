// Copyright © 2023 TXA PTE. LTD.

package db

import (
	"sdp/model"

	"gorm.io/gorm"
)

func UniqueTrade(seq uint64) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		return db.
			Model(&model.Trade{}).
			Where("sdp_sequence_number = ?", seq)
	}
}

func UniqueRejectedTrade(seq uint64) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		return db.
			Model(&model.RejectedTrade{}).
			Where("sdp_sequence_number = ?", seq)
	}
}

func UniqueOrder(seq uint64) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		return db.
			Model(&model.OrdersCanceledForSettlement{}).
			Where("sdp_sequence_number = ?", seq)
	}
}

func UniqueEvent(txHash string, eventType int32) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		return db.
			Model(&model.Event{}).
			Where("tx_hash = ? AND event_type = ?", txHash, eventType)
	}
}
