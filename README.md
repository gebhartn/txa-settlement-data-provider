<div align="center">

<img height="100px" src="https://assets.website-files.com/613fca82d8da507c85299f8c/615c90ef649f4021d9b6dba6_txa-project-logo.svg" />

# Settlement Data Provider

<a href="https://t.me/ProjectTXA"> ![Telegram](https://img.shields.io/badge/Telegram-Join-9cf?style=for-the-badge&logo=telegram) </a>
<a href="https://discord.gg/ZgGD4BKkC7"> ![Discord](https://img.shields.io/badge/discord-join-7289da?style=for-the-badge&logo=discord) </a>
<a href="">![Node Waitlist](https://img.shields.io/badge/Node%20Waitlist-signup-brightgreen?style=for-the-badge)</a>

[Summary](#summary)
•
[Quickstart](#-quickstart)
•
[Setup](#-setup)
•
[Testing](#-testing)

</div>

<div align="center">

## Summary

</div>

In the [Project TXA hybrid-DEX (hDEX)](https://www.txa.app/), trades are broadcast to a network of open-source, community operated Settlement Data Providers (SDPs), these track net obligations between traders and participate in a special consensus process to settle trades.

These SDPs are off-chain and can connect to any blockchain they choose. It's a decentralized network of special providers that are similar to blockchain miners or PoS blockchain validator nodes. In the same way that miners collect broadcasted transactions, and create blocks for a fee, SDPs collect broadcasted trades, and participate in settlements for a fee.

## 🔧 Quickstart

The fastest way to run the service is to run the dev command, if you want to give it a shot run the `make dev` command. If everything works first try, then you're good to go, but if you find that you are missing dependecies, read onto the setup section.

## 📦 Setup

Getting started with the SDP is quite simple, as most behavior is controlled with the Makefile. A list of make targets can be found via `make help`. You're required to [install GNU Make](https://www.gnu.org/software/make/) to interact with the Makefile.

#### Prerequisites

The SDP exposes a gRPC server which relies on code generation, so you may need to first [install the protobuf compiler](https://grpc.io/docs/protoc-installation/) to generate code from the proto definition, as well as the Go plugins for the protobuf compiler:

```sh
    $ go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28
    $ go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2
```

Additionally, you will need to [install the Go compiler](https://golang.org/doc/install) to build the application.

Once both of these tools are installed, you may generate and build the application via `make generate` and `make build` respectively.

#### Dependencies

In order to run locally, several extra services must be accessible to the SDP at startup. Again, several make targets are available to run these services with the required configurations set, but to do so requires you to [install docker](https://docs.docker.com/get-docker/).

The quickest way to get running is to use the `make dev` target to quickly run all the requisite Docker containers.

Individually, you must run a Postgres instance, a Redis instance, a Cliquebait instance, and a Streamer instance. Make targets are exposed for all of these individually, to see an exhaustive list of targets, use `make help`.

## ⚡Testing

In keeping with the general theme of the make targets, you may run tests via `make test`. As a caveat to, you'll additionally need to be running postgres for the tests to connect to a DB.
