// Copyright © 2023 TXA PTE. LTD.

package main

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"sdp/config"
	"sdp/constants"
	"sdp/db"
	"sdp/model"

	"gorm.io/gorm"
)

var (
	alice      = strings.ToLower("0xEE214aC2A18bCDf7F563f63a2Cbc30DdAD9DB2E4")
	bob        = strings.ToLower("0x537B304C9B632FFE222C222CD455d2bC6ae6af1f")
	eth_amount = "150000000000000000"
	dmc_amount = "202500"
	chain_id   = uint64(17001)
)

func queryObligations(pg *gorm.DB, sink, source, amount, token string, chainId uint64) ([]model.Obligation, error) {
	var result []model.Obligation
	if err := pg.Where(&model.Obligation{Sink: sink, Source: source, Amount: amount, Token: token, ChainId: chainId}).Find(&result).Error; err != nil {
		log.Println(err)
		return nil, err
	}
	return result, nil
}

func ctxAnddb() (context.Context, *gorm.DB) {
	ctx := context.TODO()
	pgURL := config.GetPgURL()

	pg, _ := db.NewDb(pgURL)
	return ctx, pg
}

func main() {
	_, pg := ctxAnddb()

	time.Sleep(5 * time.Second)

	ae, err := queryObligations(pg, bob, alice, eth_amount, constants.ZeroAddress, chain_id)
	if err != nil {
		panic(err)
	}
	if len(ae) != 1 {
		fmt.Println(ae)
		panic("Expected obligation for trade from alice to bob")
	}

	bdmc, err := queryObligations(pg, alice, bob, dmc_amount, constants.DmcAddress, chain_id)
	if err != nil {
		panic(err)
	}
	if len(bdmc) != 1 {
		fmt.Println(bdmc)
		panic("Expected obligation for trade from bob to alice")
	}
}
