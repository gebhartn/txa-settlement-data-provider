// Copyright © 2023 TXA PTE. LTD.

package indexer

import (
	"context"
	"io"
	"net"
	"time"

	"sdp/config"
	"sdp/logger"
	"sdp/proto_def"
	"sdp/queue"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/encoding/prototext"
)

type Indexer struct {
	proto_def.UnimplementedSDPServer

	closing           chan chan error
	server            *grpc.Server
	queue             *queue.Queue
	reconnectInterval time.Duration
	log               *logrus.Logger
}

func NewIndexer(queue *queue.Queue, reconnectInterval time.Duration) *Indexer {
	return &Indexer{
		queue:             queue,
		server:            grpc.NewServer(),
		closing:           make(chan chan error),
		reconnectInterval: reconnectInterval,
		log:               logger.Default,
	}
}

func (c *Indexer) RecordEvents(stream proto_def.SDP_RecordEventsServer) error {
	for {
		event, err := stream.Recv()
		if err == io.EOF {
			success := true
			return stream.SendAndClose(&proto_def.ParsedEventResponse{
				Success: &success,
			})
		}
		if err != nil {
			c.log.Error(err)
			break
		}

		b, err := prototext.Marshal(event)
		if err != nil {
			c.log.Error(err)
			break
		}
		if err := c.queue.Publish(context.Background(), config.GetBlockchainEventsKey(), string(b)); err != nil {
			c.log.Errorf("error writing to redis queue: %v", err)
		}
	}

	return nil
}

func (c *Indexer) Close() error {
	errc := make(chan error)
	c.closing <- errc
	return <-errc
}

func (c *Indexer) Start(ctx context.Context) {
	var err error
	var listener net.Listener

	go func() {
		listener, err = net.Listen("tcp", ":"+config.GetGrpcPort())
		if err != nil {
			c.log.Error(err)
			time.Sleep(c.reconnectInterval)
		}

		proto_def.RegisterSDPServer(c.server, c)
		if err := c.server.Serve(listener); err != nil {
			c.log.Error(err)
			time.Sleep(c.reconnectInterval)
		}
	}()

	for {
		select {
		case <-ctx.Done():
			return
		case errc := <-c.closing:
			c.server.GracefulStop()
			errc <- err
			return
		}
	}
}
