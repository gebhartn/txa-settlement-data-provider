// Copyright © 2023 TXA PTE. LTD.

package types

import (
	"encoding/json"
)

type AssetNetworkChainType uint

const (
	ASSET_NETWORK_CHAIN_NONE AssetNetworkChainType = iota
	ASSET_NETWORK_CHAIN_PRIV
	ASSET_NETWORK_CHAIN_TEST
	ASSET_NETOWRK_CHAIN_MAIN
)

type TradeStatus uint

const (
	TRADE_STATUS_NONE     TradeStatus = iota
	TRADE_STATUS_OPEN                 = 1
	TRADE_STATUS_EXECUTED             = 2
)

type MsgType uint

func (mt *MsgType) Decode(b []byte) (*MsgType, error) {
	t := &struct{ MsgType MsgType }{}
	err := json.Unmarshal(b, t)
	if err != nil {
		return nil, err
	}
	return &t.MsgType, nil
}

const (
	MSG_TRADE                          MsgType = 2
	MSG_ORDERS_CANCELED_FOR_SETTLEMENT MsgType = 37
)

func (mt *MsgType) String() string {
	switch *mt {
	case 2:
		return "MSG_TRADE"
	case 37:
		return "MSG_ORDERS_CANCELED_FOR_SETTLEMENT"
	default:
		return "MSG_TYPE_UNKNOWN"
	}
}

type Product struct {
	BaseChainId    uint64   `json:"baseChainId"`
	CounterChainId uint64   `json:"counterChainId"`
	CounterAsset   ApiAsset `json:"counterAsset"`
	BaseAsset      ApiAsset `json:"baseAsset"`
	AggLevels      []uint64 `json:"aggLevels"`
}

type Products map[string]*Product

func (p *Product) GetBaseAsset() string {
	return p.BaseAsset.ContractAddress
}

func (p *Product) GetCounterAsset() string {
	return p.CounterAsset.ContractAddress
}

func (p *Product) GetBaseChainId() uint64 {
	return uint64(p.BaseAsset.EvmChainId)
}

func (p *Product) GetCounterChainId() uint64 {
	return uint64(p.CounterAsset.EvmChainId)
}

func (p *Product) GetBaseAssetPrecision() int {
	return p.BaseAsset.NativePrecision
}

func NewProduct(base, counter ApiAsset, al []uint64) *Product {
	return &Product{
		BaseAsset:    base,
		CounterAsset: counter,
		AggLevels:    al,
	}
}

type ApiAsset struct {
	Id               string `json:"id"`
	AssetClass       string `json:"assetClass"`
	AssetSubtype     string `json:"assetSubtype"`
	CommonFullName   string `json:"commonFullName"`
	CommonUnitName   string `json:"commonUnitName"`
	ContractAddress  string `json:"contractAddress"`
	CreatedAt        string `json:"createdAt"`
	UpdatedAt        string `json:"updatedAt"`
	EvmChainId       int    `json:"evmChainId"`
	EvmNetworkId     int    `json:"evmNetworkId"`
	DisplayDigits    uint64 `json:"displayDigits"`
	Name             string `json:"name"`
	NetworkChainType string `json:"networkChainType"`
	NetworkType      string `json:"networkType"`
	NetworkName      string `json:"networkName"`
	Precision        int    `json:"precision"`
	NativePrecision  int    `json:"nativePrecision"`
}

type ApiProduct struct {
	Id             string
	BaseAssetId    string
	CounterAssetId string
	MinimumLotSize string
	PriceInterval  string
	AggLevels      []string
	CreatedAt      string
	UpdatedAt      string
}

type ApiProducts []ApiProduct
