// Copyright © 2023 TXA PTE. LTD.

package store

import (
	"errors"
	"fmt"

	"sdp/db"
	"sdp/logger"
	"sdp/model"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

var log = logger.Default

type TradeStore struct {
	db *gorm.DB
}

func NewTradeStore(db *gorm.DB) *TradeStore {
	return &TradeStore{db: db}
}

func (s *TradeStore) Create(trade any) error {
	if trade.(*model.Trade).Rejected() {
		return s.createRejectedTrade(&model.RejectedTrade{Trade: *trade.(*model.Trade)})
	}
	return s.createTrade(trade)
}

func (s *TradeStore) createRejectedTrade(trade any) error {
	var count int64
	if err := s.db.Scopes(db.UniqueRejectedTrade(trade.(*model.RejectedTrade).SdpSequenceNumber)).Count(&count).Error; err != nil {
		return err
	}
	if count == 0 {
		if err := s.db.Clauses(clause.OnConflict{
			DoNothing: true,
		}).Create(trade.(*model.RejectedTrade)).Error; err != nil {
			return err
		}
	}
	return nil
}

func (s *TradeStore) createTrade(trade any) error {
	var count int64
	if err := s.db.Scopes(db.UniqueTrade(trade.(*model.Trade).SdpSequenceNumber)).Count(&count).Error; err != nil {
		return err
	}
	if count == 0 {
		if err := s.db.Clauses(clause.OnConflict{
			DoNothing: true,
		}).Create(trade.(*model.Trade)).Error; err != nil {
			return err
		}
	}
	return nil
}

func (s *TradeStore) GetBySeq(seq uint64) (*model.Trade, error) {
	var m model.Trade
	if err := s.db.Where("sdp_sequence_number = ? AND processed = ?", seq, false).First(&m).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, err
	}
	return &m, nil
}

func (s *TradeStore) GetLastProcessedTrade() (*model.LastProcessedTrade, error) {
	var m model.LastProcessedTrade
	if err := s.db.First(&m).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, err
	}
	return &m, nil
}

func (s *TradeStore) UpdateLastProcessedTrade(seq uint64) error {
	lpt, err := s.GetLastProcessedTrade()
	if err != nil {
		return err
	}
	if lpt.SdpSequenceNumber != seq-1 {
		return fmt.Errorf("sequence is out of order")
	}

	return s.db.Model(&lpt).Updates(&model.LastProcessedTrade{SdpSequenceNumber: lpt.SdpSequenceNumber + 1}).Error
}

func (s *TradeStore) ProcessTrade(t *model.Trade) error {
	if err := s.db.Model(&model.Trade{}).Where(t).Update("processed", true).Error; err != nil {
		log.Error("failed to mark trade processed")
		return err
	}
	return nil
}

func (s *TradeStore) MostRecentTrade() uint64 {
	var max uint64
	s.db.Table("trades").Select("MAX(sdp_sequence_number)").Row().Scan(&max)
	return max
}

func (s *TradeStore) IsRejected(seq uint64) (bool, error) {
	var m model.RejectedTrade
	if err := s.db.Where("sdp_sequence_number = ?", seq).First(&m).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return false, nil
		}
		return false, err
	}
	fmt.Println("we found rejected trade")
	return true, nil
}
