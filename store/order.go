// Copyright © 2023 TXA PTE. LTD.

package store

import (
	"errors"

	"sdp/db"
	"sdp/model"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type OrderStore struct {
	db *gorm.DB
}

func NewOrderStore(db *gorm.DB) *OrderStore {
	return &OrderStore{db: db}
}

func (s *OrderStore) GetBySeq(seq uint64) (*model.OrdersCanceledForSettlement, error) {
	var m model.OrdersCanceledForSettlement
	if err := s.db.
		Where(map[string]any{"sdp_sequence_number": seq, "processed": false}).
		First(&m).
		Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, err
	}
	return &m, nil
}

func (s *OrderStore) FindWhereProcessed(settlementId string, chainId int) (*model.OrdersCanceledForSettlement, error) {
	var m model.OrdersCanceledForSettlement
	if err := s.db.Where(&model.OrdersCanceledForSettlement{SettlementId: settlementId, Processed: true, ChainId: chainId}).First(&m).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, err
	}
	return &m, nil
}

func (s *OrderStore) Create(order any) error {
	var count int64
	if err := s.db.Scopes(db.UniqueOrder(order.(*model.OrdersCanceledForSettlement).SdpSequenceNumber)).
		Count(&count).Error; err != nil {
		return err
	}
	if count != 0 {
		return nil
	}
	return s.db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).Create(order.(*model.OrdersCanceledForSettlement)).Error
}

func (s *OrderStore) MostRecentOrder() uint64 {
	var max uint64
	s.db.Table("orders_canceled_for_settlements").Select("MAX(sdp_sequence_number)").Row().Scan(&max)
	return max
}

func (s *OrderStore) GetLastProcessedTrade() (*model.LastProcessedTrade, error) {
	var m model.LastProcessedTrade
	if err := s.db.First(&m).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, err
	}
	return &m, nil
}

func (s *OrderStore) ProcessOrder(t *model.OrdersCanceledForSettlement) error {
	if err := s.db.Model(&model.OrdersCanceledForSettlement{}).
		Where(t).
		Update("processed", true).
		Error; err != nil {
		log.Error("failed to mark order processed")
		return err
	}
	return nil
}
