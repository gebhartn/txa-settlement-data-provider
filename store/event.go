// Copyright © 2023 TXA PTE. LTD.

package store

import (
	"errors"

	"sdp/db"
	"sdp/model"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type EventStore struct {
	db *gorm.DB
}

func NewEventStore(db *gorm.DB) *EventStore {
	return &EventStore{db: db}
}

func (s *EventStore) GetByTxHash(tx string) (*model.Event, error) {
	var m model.Event
	if err := s.db.Where("tx_hash = ?", tx).Find(&m).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, err
	}
	return &m, nil
}

func (s *EventStore) GetBySettlementId(settlementId string, chainId uint64, token string) (*model.Event, error) {
	var m model.Event
	query := map[string]any{
		"settlement_id": settlementId,
		"chain_id":      chainId,
		"token":         token,
		"processed":     true,
	}
	if err := s.db.Where(query).First(&m).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, err
	}
	return &m, nil
}

func (s *EventStore) GetUnprocessedEvents() (*[]model.Event, error) {
	var m []model.Event
	if err := s.db.Where("processed = ?", false).Find(&m).Error; err != nil {
		log.Error("failed to get events")
		return nil, err
	}
	return &m, nil
}

func (s *EventStore) ProcessEvent(e *model.Event) error {
	if err := s.db.Model(&model.Event{}).Where(e).Update("processed", true).Error; err != nil {
		log.Error("failed to save event")
		return err
	}
	return nil
}

func (s *EventStore) CompleteEvent(e *model.Event) error {
	if err := s.db.Model(&model.Event{}).Where(e).Update("completed", true).Error; err != nil {
		return err
	}
	return nil
}

func (s *EventStore) Create(event any) error {
	var count int64
	if err := s.db.Scopes(db.UniqueEvent(event.(*model.Event).TxHash, event.(*model.Event).EventType)).
		Count(&count).Error; err != nil {
		return err
	}
	if count != 0 {
		return nil
	}
	return s.db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).Create(event.(*model.Event)).Error
}
