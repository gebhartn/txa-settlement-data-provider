// Copyright © 2023 TXA PTE. LTD.
// After getting the .abi files from the smart contracts, run the script
// below to generate go bindings for those contracts.
//
//go:generate abigen --abi ./IdentityRegistry.abi --pkg abis --out identity_registry.go --type IdentityRegistry
//go:generate abigen --abi ./SettlementDataConsensus.abi --pkg abis --out consensus.go --type Consensus
//go:generate abigen --abi ./SettlementCoordination.abi --pkg abis --out coordinator.go --type Coordinator
//go:generate abigen --abi ./Rollup.abi --pkg abis --out rollup.go --type Rollup
package abis
