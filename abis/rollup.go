// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package abis

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// StateUpdateLibrarySignedStateUpdate is an auto generated low-level Go binding around an user-defined struct.
type StateUpdateLibrarySignedStateUpdate struct {
	StateUpdate StateUpdateLibraryStateUpdate
	V           uint8
	R           [32]byte
	S           [32]byte
}

// StateUpdateLibraryStateUpdate is an auto generated low-level Go binding around an user-defined struct.
type StateUpdateLibraryStateUpdate struct {
	TypeIdentifier         uint8
	SequenceId             *big.Int
	ParticipatingInterface common.Address
	StructData             []byte
}

// StateUpdateLibraryUTXO is an auto generated low-level Go binding around an user-defined struct.
type StateUpdateLibraryUTXO struct {
	Trader                 common.Address
	Amount                 *big.Int
	StateUpdateId          *big.Int
	ParentUtxo             [32]byte
	DepositUtxo            [32]byte
	ParticipatingInterface common.Address
	Asset                  common.Address
	ChainId                *big.Int
}

// RollupMetaData contains all meta data concerning the Rollup contract.
var RollupMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_participatingInterface\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_manager\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"inputs\":[],\"name\":\"CALLER_NOT_VALIDATOR\",\"type\":\"error\"},{\"inputs\":[],\"name\":\"EMPTY_STATE_ROOT\",\"type\":\"error\"},{\"inputs\":[],\"name\":\"EPOCH_NOT_CONFIRMED\",\"type\":\"error\"},{\"inputs\":[],\"name\":\"INPUTS_HASH_MISMATCH_SETTLEMENT\",\"type\":\"error\"},{\"inputs\":[],\"name\":\"INPUTS_LENGTH_MISMATCH_SETTLEMENT\",\"type\":\"error\"},{\"inputs\":[],\"name\":\"INPUT_PARAMS_MISMATCH_SETTLEMENT\",\"type\":\"error\"},{\"inputs\":[],\"name\":\"INVALID_PROOF_SETTLEMENT\",\"type\":\"error\"},{\"inputs\":[],\"name\":\"INVALID_REQUEST_SETTLEMENT\",\"type\":\"error\"},{\"inputs\":[],\"name\":\"INVALID_SEQUENCE_SETTLEMENT\",\"type\":\"error\"},{\"inputs\":[],\"name\":\"INVALID_STATE_UPDATE_SETTLEMENT\",\"type\":\"error\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"Id\",\"name\":\"id\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"requester\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"cleared\",\"type\":\"uint256\"}],\"name\":\"ObligationsWritten\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"Id\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"confirmedStateRoot\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"epoch\",\"outputs\":[{\"internalType\":\"Id\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_epoch\",\"type\":\"uint256\"}],\"name\":\"getConfirmedStateRoot\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"root\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getCurrentEpoch\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"lastSettlementIdProcessed\",\"outputs\":[{\"internalType\":\"Id\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"nextRequestId\",\"outputs\":[{\"internalType\":\"Id\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"_stateRoot\",\"type\":\"bytes32\"},{\"components\":[{\"components\":[{\"internalType\":\"uint8\",\"name\":\"typeIdentifier\",\"type\":\"uint8\"},{\"internalType\":\"Id\",\"name\":\"sequenceId\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"participatingInterface\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"structData\",\"type\":\"bytes\"}],\"internalType\":\"structStateUpdateLibrary.StateUpdate\",\"name\":\"stateUpdate\",\"type\":\"tuple\"},{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"}],\"internalType\":\"structStateUpdateLibrary.SignedStateUpdate\",\"name\":\"_settlementAcknowledgement\",\"type\":\"tuple\"},{\"internalType\":\"bytes32[]\",\"name\":\"_proof\",\"type\":\"bytes32[]\"},{\"components\":[{\"internalType\":\"address\",\"name\":\"trader\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"stateUpdateId\",\"type\":\"uint256\"},{\"internalType\":\"bytes32\",\"name\":\"parentUtxo\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"depositUtxo\",\"type\":\"bytes32\"},{\"internalType\":\"address\",\"name\":\"participatingInterface\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"asset\",\"type\":\"address\"},{\"internalType\":\"Id\",\"name\":\"chainId\",\"type\":\"uint256\"}],\"internalType\":\"structStateUpdateLibrary.UTXO[]\",\"name\":\"_inputs\",\"type\":\"tuple[]\"}],\"name\":\"processSettlement\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"_stateRoot\",\"type\":\"bytes32\"}],\"name\":\"proposeStateRoot\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"Id\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"proposedStateRoot\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"requestSettlement\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

// RollupABI is the input ABI used to generate the binding from.
// Deprecated: Use RollupMetaData.ABI instead.
var RollupABI = RollupMetaData.ABI

// Rollup is an auto generated Go binding around an Ethereum contract.
type Rollup struct {
	RollupCaller     // Read-only binding to the contract
	RollupTransactor // Write-only binding to the contract
	RollupFilterer   // Log filterer for contract events
}

// RollupCaller is an auto generated read-only Go binding around an Ethereum contract.
type RollupCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// RollupTransactor is an auto generated write-only Go binding around an Ethereum contract.
type RollupTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// RollupFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type RollupFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// RollupSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type RollupSession struct {
	Contract     *Rollup           // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// RollupCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type RollupCallerSession struct {
	Contract *RollupCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// RollupTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type RollupTransactorSession struct {
	Contract     *RollupTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// RollupRaw is an auto generated low-level Go binding around an Ethereum contract.
type RollupRaw struct {
	Contract *Rollup // Generic contract binding to access the raw methods on
}

// RollupCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type RollupCallerRaw struct {
	Contract *RollupCaller // Generic read-only contract binding to access the raw methods on
}

// RollupTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type RollupTransactorRaw struct {
	Contract *RollupTransactor // Generic write-only contract binding to access the raw methods on
}

// NewRollup creates a new instance of Rollup, bound to a specific deployed contract.
func NewRollup(address common.Address, backend bind.ContractBackend) (*Rollup, error) {
	contract, err := bindRollup(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Rollup{RollupCaller: RollupCaller{contract: contract}, RollupTransactor: RollupTransactor{contract: contract}, RollupFilterer: RollupFilterer{contract: contract}}, nil
}

// NewRollupCaller creates a new read-only instance of Rollup, bound to a specific deployed contract.
func NewRollupCaller(address common.Address, caller bind.ContractCaller) (*RollupCaller, error) {
	contract, err := bindRollup(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &RollupCaller{contract: contract}, nil
}

// NewRollupTransactor creates a new write-only instance of Rollup, bound to a specific deployed contract.
func NewRollupTransactor(address common.Address, transactor bind.ContractTransactor) (*RollupTransactor, error) {
	contract, err := bindRollup(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &RollupTransactor{contract: contract}, nil
}

// NewRollupFilterer creates a new log filterer instance of Rollup, bound to a specific deployed contract.
func NewRollupFilterer(address common.Address, filterer bind.ContractFilterer) (*RollupFilterer, error) {
	contract, err := bindRollup(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &RollupFilterer{contract: contract}, nil
}

// bindRollup binds a generic wrapper to an already deployed contract.
func bindRollup(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(RollupABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Rollup *RollupRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Rollup.Contract.RollupCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Rollup *RollupRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Rollup.Contract.RollupTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Rollup *RollupRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Rollup.Contract.RollupTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Rollup *RollupCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Rollup.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Rollup *RollupTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Rollup.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Rollup *RollupTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Rollup.Contract.contract.Transact(opts, method, params...)
}

// ConfirmedStateRoot is a free data retrieval call binding the contract method 0x48a8c921.
//
// Solidity: function confirmedStateRoot(uint256 ) view returns(bytes32)
func (_Rollup *RollupCaller) ConfirmedStateRoot(opts *bind.CallOpts, arg0 *big.Int) ([32]byte, error) {
	var out []interface{}
	err := _Rollup.contract.Call(opts, &out, "confirmedStateRoot", arg0)

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

// ConfirmedStateRoot is a free data retrieval call binding the contract method 0x48a8c921.
//
// Solidity: function confirmedStateRoot(uint256 ) view returns(bytes32)
func (_Rollup *RollupSession) ConfirmedStateRoot(arg0 *big.Int) ([32]byte, error) {
	return _Rollup.Contract.ConfirmedStateRoot(&_Rollup.CallOpts, arg0)
}

// ConfirmedStateRoot is a free data retrieval call binding the contract method 0x48a8c921.
//
// Solidity: function confirmedStateRoot(uint256 ) view returns(bytes32)
func (_Rollup *RollupCallerSession) ConfirmedStateRoot(arg0 *big.Int) ([32]byte, error) {
	return _Rollup.Contract.ConfirmedStateRoot(&_Rollup.CallOpts, arg0)
}

// Epoch is a free data retrieval call binding the contract method 0x900cf0cf.
//
// Solidity: function epoch() view returns(uint256)
func (_Rollup *RollupCaller) Epoch(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Rollup.contract.Call(opts, &out, "epoch")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Epoch is a free data retrieval call binding the contract method 0x900cf0cf.
//
// Solidity: function epoch() view returns(uint256)
func (_Rollup *RollupSession) Epoch() (*big.Int, error) {
	return _Rollup.Contract.Epoch(&_Rollup.CallOpts)
}

// Epoch is a free data retrieval call binding the contract method 0x900cf0cf.
//
// Solidity: function epoch() view returns(uint256)
func (_Rollup *RollupCallerSession) Epoch() (*big.Int, error) {
	return _Rollup.Contract.Epoch(&_Rollup.CallOpts)
}

// GetConfirmedStateRoot is a free data retrieval call binding the contract method 0x4cc4f7c0.
//
// Solidity: function getConfirmedStateRoot(uint256 _epoch) view returns(bytes32 root)
func (_Rollup *RollupCaller) GetConfirmedStateRoot(opts *bind.CallOpts, _epoch *big.Int) ([32]byte, error) {
	var out []interface{}
	err := _Rollup.contract.Call(opts, &out, "getConfirmedStateRoot", _epoch)

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

// GetConfirmedStateRoot is a free data retrieval call binding the contract method 0x4cc4f7c0.
//
// Solidity: function getConfirmedStateRoot(uint256 _epoch) view returns(bytes32 root)
func (_Rollup *RollupSession) GetConfirmedStateRoot(_epoch *big.Int) ([32]byte, error) {
	return _Rollup.Contract.GetConfirmedStateRoot(&_Rollup.CallOpts, _epoch)
}

// GetConfirmedStateRoot is a free data retrieval call binding the contract method 0x4cc4f7c0.
//
// Solidity: function getConfirmedStateRoot(uint256 _epoch) view returns(bytes32 root)
func (_Rollup *RollupCallerSession) GetConfirmedStateRoot(_epoch *big.Int) ([32]byte, error) {
	return _Rollup.Contract.GetConfirmedStateRoot(&_Rollup.CallOpts, _epoch)
}

// GetCurrentEpoch is a free data retrieval call binding the contract method 0xb97dd9e2.
//
// Solidity: function getCurrentEpoch() view returns(uint256)
func (_Rollup *RollupCaller) GetCurrentEpoch(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Rollup.contract.Call(opts, &out, "getCurrentEpoch")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetCurrentEpoch is a free data retrieval call binding the contract method 0xb97dd9e2.
//
// Solidity: function getCurrentEpoch() view returns(uint256)
func (_Rollup *RollupSession) GetCurrentEpoch() (*big.Int, error) {
	return _Rollup.Contract.GetCurrentEpoch(&_Rollup.CallOpts)
}

// GetCurrentEpoch is a free data retrieval call binding the contract method 0xb97dd9e2.
//
// Solidity: function getCurrentEpoch() view returns(uint256)
func (_Rollup *RollupCallerSession) GetCurrentEpoch() (*big.Int, error) {
	return _Rollup.Contract.GetCurrentEpoch(&_Rollup.CallOpts)
}

// LastSettlementIdProcessed is a free data retrieval call binding the contract method 0x4698bb22.
//
// Solidity: function lastSettlementIdProcessed() view returns(uint256)
func (_Rollup *RollupCaller) LastSettlementIdProcessed(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Rollup.contract.Call(opts, &out, "lastSettlementIdProcessed")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// LastSettlementIdProcessed is a free data retrieval call binding the contract method 0x4698bb22.
//
// Solidity: function lastSettlementIdProcessed() view returns(uint256)
func (_Rollup *RollupSession) LastSettlementIdProcessed() (*big.Int, error) {
	return _Rollup.Contract.LastSettlementIdProcessed(&_Rollup.CallOpts)
}

// LastSettlementIdProcessed is a free data retrieval call binding the contract method 0x4698bb22.
//
// Solidity: function lastSettlementIdProcessed() view returns(uint256)
func (_Rollup *RollupCallerSession) LastSettlementIdProcessed() (*big.Int, error) {
	return _Rollup.Contract.LastSettlementIdProcessed(&_Rollup.CallOpts)
}

// NextRequestId is a free data retrieval call binding the contract method 0x6a84a985.
//
// Solidity: function nextRequestId() view returns(uint256)
func (_Rollup *RollupCaller) NextRequestId(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Rollup.contract.Call(opts, &out, "nextRequestId")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// NextRequestId is a free data retrieval call binding the contract method 0x6a84a985.
//
// Solidity: function nextRequestId() view returns(uint256)
func (_Rollup *RollupSession) NextRequestId() (*big.Int, error) {
	return _Rollup.Contract.NextRequestId(&_Rollup.CallOpts)
}

// NextRequestId is a free data retrieval call binding the contract method 0x6a84a985.
//
// Solidity: function nextRequestId() view returns(uint256)
func (_Rollup *RollupCallerSession) NextRequestId() (*big.Int, error) {
	return _Rollup.Contract.NextRequestId(&_Rollup.CallOpts)
}

// ProposedStateRoot is a free data retrieval call binding the contract method 0xc98bdc03.
//
// Solidity: function proposedStateRoot(uint256 ) view returns(bytes32)
func (_Rollup *RollupCaller) ProposedStateRoot(opts *bind.CallOpts, arg0 *big.Int) ([32]byte, error) {
	var out []interface{}
	err := _Rollup.contract.Call(opts, &out, "proposedStateRoot", arg0)

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

// ProposedStateRoot is a free data retrieval call binding the contract method 0xc98bdc03.
//
// Solidity: function proposedStateRoot(uint256 ) view returns(bytes32)
func (_Rollup *RollupSession) ProposedStateRoot(arg0 *big.Int) ([32]byte, error) {
	return _Rollup.Contract.ProposedStateRoot(&_Rollup.CallOpts, arg0)
}

// ProposedStateRoot is a free data retrieval call binding the contract method 0xc98bdc03.
//
// Solidity: function proposedStateRoot(uint256 ) view returns(bytes32)
func (_Rollup *RollupCallerSession) ProposedStateRoot(arg0 *big.Int) ([32]byte, error) {
	return _Rollup.Contract.ProposedStateRoot(&_Rollup.CallOpts, arg0)
}

// ProcessSettlement is a paid mutator transaction binding the contract method 0x29237024.
//
// Solidity: function processSettlement(bytes32 _stateRoot, ((uint8,uint256,address,bytes),uint8,bytes32,bytes32) _settlementAcknowledgement, bytes32[] _proof, (address,uint256,uint256,bytes32,bytes32,address,address,uint256)[] _inputs) returns()
func (_Rollup *RollupTransactor) ProcessSettlement(opts *bind.TransactOpts, _stateRoot [32]byte, _settlementAcknowledgement StateUpdateLibrarySignedStateUpdate, _proof [][32]byte, _inputs []StateUpdateLibraryUTXO) (*types.Transaction, error) {
	return _Rollup.contract.Transact(opts, "processSettlement", _stateRoot, _settlementAcknowledgement, _proof, _inputs)
}

// ProcessSettlement is a paid mutator transaction binding the contract method 0x29237024.
//
// Solidity: function processSettlement(bytes32 _stateRoot, ((uint8,uint256,address,bytes),uint8,bytes32,bytes32) _settlementAcknowledgement, bytes32[] _proof, (address,uint256,uint256,bytes32,bytes32,address,address,uint256)[] _inputs) returns()
func (_Rollup *RollupSession) ProcessSettlement(_stateRoot [32]byte, _settlementAcknowledgement StateUpdateLibrarySignedStateUpdate, _proof [][32]byte, _inputs []StateUpdateLibraryUTXO) (*types.Transaction, error) {
	return _Rollup.Contract.ProcessSettlement(&_Rollup.TransactOpts, _stateRoot, _settlementAcknowledgement, _proof, _inputs)
}

// ProcessSettlement is a paid mutator transaction binding the contract method 0x29237024.
//
// Solidity: function processSettlement(bytes32 _stateRoot, ((uint8,uint256,address,bytes),uint8,bytes32,bytes32) _settlementAcknowledgement, bytes32[] _proof, (address,uint256,uint256,bytes32,bytes32,address,address,uint256)[] _inputs) returns()
func (_Rollup *RollupTransactorSession) ProcessSettlement(_stateRoot [32]byte, _settlementAcknowledgement StateUpdateLibrarySignedStateUpdate, _proof [][32]byte, _inputs []StateUpdateLibraryUTXO) (*types.Transaction, error) {
	return _Rollup.Contract.ProcessSettlement(&_Rollup.TransactOpts, _stateRoot, _settlementAcknowledgement, _proof, _inputs)
}

// ProposeStateRoot is a paid mutator transaction binding the contract method 0x706efcd0.
//
// Solidity: function proposeStateRoot(bytes32 _stateRoot) returns()
func (_Rollup *RollupTransactor) ProposeStateRoot(opts *bind.TransactOpts, _stateRoot [32]byte) (*types.Transaction, error) {
	return _Rollup.contract.Transact(opts, "proposeStateRoot", _stateRoot)
}

// ProposeStateRoot is a paid mutator transaction binding the contract method 0x706efcd0.
//
// Solidity: function proposeStateRoot(bytes32 _stateRoot) returns()
func (_Rollup *RollupSession) ProposeStateRoot(_stateRoot [32]byte) (*types.Transaction, error) {
	return _Rollup.Contract.ProposeStateRoot(&_Rollup.TransactOpts, _stateRoot)
}

// ProposeStateRoot is a paid mutator transaction binding the contract method 0x706efcd0.
//
// Solidity: function proposeStateRoot(bytes32 _stateRoot) returns()
func (_Rollup *RollupTransactorSession) ProposeStateRoot(_stateRoot [32]byte) (*types.Transaction, error) {
	return _Rollup.Contract.ProposeStateRoot(&_Rollup.TransactOpts, _stateRoot)
}

// RequestSettlement is a paid mutator transaction binding the contract method 0x3003f147.
//
// Solidity: function requestSettlement(address , address ) returns(uint256)
func (_Rollup *RollupTransactor) RequestSettlement(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address) (*types.Transaction, error) {
	return _Rollup.contract.Transact(opts, "requestSettlement", arg0, arg1)
}

// RequestSettlement is a paid mutator transaction binding the contract method 0x3003f147.
//
// Solidity: function requestSettlement(address , address ) returns(uint256)
func (_Rollup *RollupSession) RequestSettlement(arg0 common.Address, arg1 common.Address) (*types.Transaction, error) {
	return _Rollup.Contract.RequestSettlement(&_Rollup.TransactOpts, arg0, arg1)
}

// RequestSettlement is a paid mutator transaction binding the contract method 0x3003f147.
//
// Solidity: function requestSettlement(address , address ) returns(uint256)
func (_Rollup *RollupTransactorSession) RequestSettlement(arg0 common.Address, arg1 common.Address) (*types.Transaction, error) {
	return _Rollup.Contract.RequestSettlement(&_Rollup.TransactOpts, arg0, arg1)
}

// RollupObligationsWrittenIterator is returned from FilterObligationsWritten and is used to iterate over the raw logs and unpacked data for ObligationsWritten events raised by the Rollup contract.
type RollupObligationsWrittenIterator struct {
	Event *RollupObligationsWritten // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *RollupObligationsWrittenIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(RollupObligationsWritten)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(RollupObligationsWritten)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *RollupObligationsWrittenIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *RollupObligationsWrittenIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// RollupObligationsWritten represents a ObligationsWritten event raised by the Rollup contract.
type RollupObligationsWritten struct {
	Id        *big.Int
	Requester common.Address
	Token     common.Address
	Cleared   *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterObligationsWritten is a free log retrieval operation binding the contract event 0x2fbe32bf5c43587f241785776e03af9725712c4d9349d5d487e0b080554759cf.
//
// Solidity: event ObligationsWritten(uint256 id, address requester, address token, uint256 cleared)
func (_Rollup *RollupFilterer) FilterObligationsWritten(opts *bind.FilterOpts) (*RollupObligationsWrittenIterator, error) {

	logs, sub, err := _Rollup.contract.FilterLogs(opts, "ObligationsWritten")
	if err != nil {
		return nil, err
	}
	return &RollupObligationsWrittenIterator{contract: _Rollup.contract, event: "ObligationsWritten", logs: logs, sub: sub}, nil
}

// WatchObligationsWritten is a free log subscription operation binding the contract event 0x2fbe32bf5c43587f241785776e03af9725712c4d9349d5d487e0b080554759cf.
//
// Solidity: event ObligationsWritten(uint256 id, address requester, address token, uint256 cleared)
func (_Rollup *RollupFilterer) WatchObligationsWritten(opts *bind.WatchOpts, sink chan<- *RollupObligationsWritten) (event.Subscription, error) {

	logs, sub, err := _Rollup.contract.WatchLogs(opts, "ObligationsWritten")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(RollupObligationsWritten)
				if err := _Rollup.contract.UnpackLog(event, "ObligationsWritten", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseObligationsWritten is a log parse operation binding the contract event 0x2fbe32bf5c43587f241785776e03af9725712c4d9349d5d487e0b080554759cf.
//
// Solidity: event ObligationsWritten(uint256 id, address requester, address token, uint256 cleared)
func (_Rollup *RollupFilterer) ParseObligationsWritten(log types.Log) (*RollupObligationsWritten, error) {
	event := new(RollupObligationsWritten)
	if err := _Rollup.contract.UnpackLog(event, "ObligationsWritten", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
