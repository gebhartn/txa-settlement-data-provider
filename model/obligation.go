// Copyright © 2023 TXA PTE. LTD.

package model

import (
	"gorm.io/gorm"
)

type obligationStatus string

const (
	STAGED    obligationStatus = "STAGED"
	SUBMITTED obligationStatus = "SUBMITTED"
	REPORTED  obligationStatus = "REPORTED"
	WRITTEN   obligationStatus = "WRITTEN"
)

type Obligation struct {
	gorm.Model
	Allocated    bool
	Amount       string
	Sink         string
	Source       string
	Token        string
	Status       obligationStatus `gorm:"default:STAGED"`
	SettlementId string
	ChainId      uint64
}

type Sortable []Obligation

func (o Sortable) Len() int           { return len(o) }
func (o Sortable) Less(i, j int) bool { return o[i].Amount < o[j].Amount }
func (o Sortable) Swap(i, j int)      { o[i], o[j] = o[j], o[i] }
