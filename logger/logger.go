// Copyright © 2023 TXA PTE. LTD.

package logger

import (
	"sync"
	"time"

	"sdp/config"

	n "github.com/antonfisher/nested-logrus-formatter"
	"github.com/sirupsen/logrus"
)

var defaultLogger sync.Once

var Default = makeDefaultLogger()

func makeDefaultLogger() *logrus.Logger {
	var l *logrus.Logger
	defaultLogger.Do(func() {
		l = logrus.New()
		if config.GetAppEnv() == "production" {
			l.SetFormatter(&logrus.JSONFormatter{TimestampFormat: time.RFC3339Nano})
		} else {
			l.SetFormatter(&n.Formatter{
				HideKeys:    true,
				FieldsOrder: []string{"component", "category"},
			})
		}
	})
	return l
}
