// Copyright © 2023 TXA PTE. LTD.

package cache

import (
	"context"
	"fmt"

	"sdp/logger"
	"sdp/model"
	"sdp/queue"

	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
)

type Cache struct {
	queue *queue.Queue
	log   logrus.Logger
}

func NewCache(queue *queue.Queue) *Cache {
	return &Cache{
		queue: queue,
		log:   *logger.Default,
	}
}

func (c *Cache) Subscribe(ctx context.Context, key string) *redis.PubSub {
	return c.queue.Subscribe(ctx, key)
}

func (c *Cache) Create(value any) error {
	switch value := value.(type) {
	case *model.Trade:
		return c.queue.Set(context.Background(), fmt.Sprintf("trade:%d", value.SdpSequenceNumber), value)
	case *model.OrdersCanceledForSettlement:
		return c.queue.Set(context.Background(), fmt.Sprintf("order:%d", value.SdpSequenceNumber), value)
	case *model.Event:
		return c.queue.Set(context.Background(), value.TxHash, value)
	default:
		c.log.Warnf("cannot cache message type: %v", value)
		return nil
	}
}
